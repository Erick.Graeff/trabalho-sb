1) O nome do nosso arquivo Assembly e de nossa biblioteca são heapManager.s e heapManager.h, respectivamente 

2) Os nossos arquivos estão em diretórios específicos:
    - ./src/heapManager.s
    - ./include/heapManager.h

3) O nosso makefile utiliza como TARGET = "avalia"

4) A implementação da função alocaMem() foi feita de forma que um novo nodo é criado quando houver uma sobra maior do que 16 bytes, por exemplo:
(a = (void *) alocaMem(20))
################++++++++++++++++++++

(liberaMem(a))
################--------------------

(a = (void *) alocaMem(2))
################++################--

OBS.: o código foi testado em 3 máquinas diferentes, duas pessoais e no dinf