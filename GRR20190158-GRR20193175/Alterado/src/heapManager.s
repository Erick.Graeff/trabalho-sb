.section .data
    BASE_HEAP   : .quad 0
    TOP_HEAP    : .quad 0
    fmt_ld      : .string "%ld\n"
    STR_START   : .string "Alocador Inciado\n"
    STR_END     : .string "Alocador Finalizado\n"
    STR_INFOS   : .string "#"
    STR_BUSY    : .string "+"
    STR_FREE    : .string "-"
    STR_ENDL    : .string "\n"
 
.section .bss
    .equ FREE, 0
    .equ BUSY, 1

.section .text
    .globl iniciaAlocador
    .globl finalizaAlocador
    .globl alocaMem
    .globl liberaMem
    .globl imprimeMapa

iniciaAlocador:
    pushq %rbp                              # salva %rbp
    movq %rsp, %rbp                         # atualiza %rbp
    movq $0, %rdi                           # %rax = atual brk
    movq $12, %rax                          # serviço brk
    syscall
    movq %rax, BASE_HEAP                    # BASE_HEAP = %rax
    movq %rax, TOP_HEAP                     # TOP_HEAP  = %rax
    movq $(STR_START), %rdi
    call printf
    popq %rbp                               # restaura %rbp
    ret                                     # restaura %rip

finalizaAlocador:
    pushq %rbp                              # salva %rbp
    movq %rsp, %rbp                         # atualiza %rbp 
    movq $(STR_END), %rdi
    call printf
    movq $12, %rax                          # serviço brk
    movq BASE_HEAP, %rdi                    # brk = BASE_HEAP
    syscall 
    popq %rbp                               # restaura %rbp
    ret                                     # restaura %rip

alocaMem:
    pushq %rbp                              # salva %rbp
    movq %rsp, %rbp                         # atualiza %rbp 
    movq %rdi, %r12                         # %r12 = num_bytes
    movq $0, %r14                           # %r14 = maior_bloco = 0
    movq BASE_HEAP, %r13                    # %r13 = BASE_HEAP
    cmpq TOP_HEAP, %r13                     # if(BASE_HEAP == TOP_HEAP)
    jne iterList                            #   execute iterList
    
    insert_last:
        addq $16, %r12                      # %r12 = num_bytes + 16
        addq %r12, TOP_HEAP                 # TOP_HEAP desloca em %r12 bytes 
        movq $12, %rax                      # serviço brk
        movq TOP_HEAP, %rdi                 # brk = TOP_HEAP
        syscall
        movq $BUSY, (%r13)                  # nodo_status = BUSY
        subq $16, %r12                      # %r12 = num_bytes
        movq %r12, 8(%r13)                  # nodo_tamanho_bloco = num_bytes
        jmp end_insert

    iterList:                               # for {
        cmpq $BUSY, (%r13)                  #   if(nodo_status != BUSY)
        jne checkBigger                     #       execute fillNode
        iterList_continue:
        movq $16, %r15                      #   %r15 = 16
        addq 8(%r13), %r15                  #   %r15 = desloc_nodo_seguinte
        addq %r15, %r13                     #   nodo = nodo_seguinte
        cmpq TOP_HEAP, %r13                 #   if (nodo == TOP_HEAP)
        je checkInsert                      #       execute insert_last
        jmp iterList                        # }

    checkBigger:
        cmpq %r14, 8(%r13)                  # if(nodo_tamanho_bloco <= maior_bloco)
        jle iterList_continue               #   execute iterList_continue
        movq 8(%r13), %r14                  # maior_bloco = nodo_tamanho_bloco
        movq %r13, %rbx                     # %rbx = &maior_nodo
        jmp iterList_continue               # return

    checkInsert:
        cmpq %r14, %r12                     # if (num_bytes > maior_bloco)
        jg  insert_last                     #   execute insert_last
        movq %rbx, %r13                     # nodo = &maior_nodo

    fillNode:
        movq $BUSY, (%r13)                  # nodo_status = BUSY
        movq 8(%r13), %r14                  # %r14 = nodo_tamanho_bloco
        subq %r12, %r14                     # %r14 = bytes_extras
        cmpq $16, %r14                      # if (bytes_extras > 16)
        jg createNode                       #   execute createNode
        jmp end_insert

    createNode:
        movq %r12, 8(%r13)                  # nodo_tamanho_bloco = num_bytes
        addq $16, %r12                      # %r12 = desloc_nodo_seguinte
        movq %r13, %r15                     # %r15 = nodo
        addq %r12, %r15                     # nodo = novo_nodo
        movq $FREE, (%r15)                  # novo_nodo_status = FREE
        subq $16, %r14
        movq %r14, 8(%r15)                  # novo_nodo_tamanho = bytes_extras
    
    end_insert:
    addq $16, %r13                          # %r13 = &nodo_bloco
    movq %r13, %rax                         # return (&nodo_bloco)
    popq %rbp                               # restaura %rbp
    ret                                     # restaura %rip

liberaMem:
    pushq %rbp                              # salva %rbp
    movq %rsp, %rbp                         # atualiza %rbp
    movq %rdi, %r12                         # %r12 = nodo_bloco
    movq BASE_HEAP, %r13                    # %r13 = BASE_HEAP

    iterEnd:
        cmpq TOP_HEAP, %r13                 # if (nodo = TOP_HEAP)
        je fail_liberaMem                   #   execute fail_liberaMem
        movq %r13, %r14                     # %r14 = nodo
        addq $16, %r14                      # %r14 = *bloco_nodo
        movq 8(%r13), %r15
        addq $16, %r15
        addq %r15, %r13
        cmpq %r14, %r12                     # if(ponteiro_passado != *bloco_nodo)
        jne iterEnd                         #   execute iterEnd

    movq $FREE, -16(%r12)                   # nodo_status = FREE
    movq BASE_HEAP, %r13                    # %r13 = nodo              

    merge_loop:
        movq 8(%r13), %r15                  # %r15 = nodo_tamanho_bloco
        addq $16, %r15                      # %r15 = desloc_nodo_seguinte
        cmpq $FREE, (%r13)                  # if (nodo_status == FREE)
        je merge                            #   execute merge
        next_busy:
        addq %r15, %r13                     # nodo = nodo_seguinte
        cmpq TOP_HEAP, %r13                 # if (nodo == TOP_HEAP)
        je end_merge_loop                   #   exit
        jmp merge_loop                 

    merge:
        movq %r13, %r14                     # %r14 = nodo
        addq %r15, %r14                     # %r14 = nodo_seguinte
        cmpq TOP_HEAP, %r14                 # if (nodo_seguinte == TOP_HEAP)
        je end_merge_loop                   #   exit
        cmpq $BUSY, (%r14)                  # if (nodo_seguinte_status == BUSY)
        je next_busy                        #   exit merge
        movq 8(%r14), %r15                  # %r15 = nodo_seguinte_tamanho
        addq $16, %r15                      # %r15 = nodo_seguinte_tamanho + 16
        addq %r15, 8(%r13)                  # nodo_tamanho_bloco += %r15
        jmp merge_loop 

    fail_liberaMem:
        movq $0, %rax                       # return 0
        jmp return_main

    end_merge_loop:
        movq $1, %rax                       # return 1

    return_main:
    popq %rbp                               # restaura %rbp
    ret                                     # restaura %rip

imprimeMapa:
    pushq %rbp                              # salva %rbp
    movq %rsp, %rbp                         # atualiza %rbp 
    movq BASE_HEAP, %r13                    # %r13 = nodo

    while:                                  # do {
        cmpq TOP_HEAP, %r13                     #   if(nodo == TOP_HEAP) 
        je end_while                        #       exit

        movq $0, %r12                       #   %r12 = 0
        for_info:                           #   for {
            cmpq $16, %r12                  #       if(%r12 == 16)
            je end_for_info                 #           exit
            movq $(STR_INFOS), %rdi         #
            call printf                     #       imprime '#'
            addq $1, %r12                   #       %r12++
            jmp for_info                    #
        end_for_info:                       #   }

        movq $0, %r12                       #   %r12 = 0
        cmpq $BUSY, (%r13)                  #   if (nodo_status == BUSY)
        je block_busy                       #       execute block_busy
        movq $(STR_FREE), %r15              #   %r15 = '-'   
        jmp for_block          
        block_busy:
        movq $(STR_BUSY), %r15              #   %r15 = '+'

        for_block:                          #   for {
            cmpq 8(%r13), %r12              #        if(%r12 >= nodo_tamanho_bloco)
            jge end_for_block               #           exit   
            movq %r15, %rdi
            call printf                     #       imprime '-' ou '+'
            movq $0, %rdi                   #   OBS: para valores grandes, precisamos fazer
            call fflush                     #   um flush no 'buffer' do printf
            addq $1, %r12                   #       %r12++
            jmp for_block                   #
        end_for_block:                      #   }
        
        movq 8(%r13), %r12                  #   %r12 = nodo_tamanho_bloco
        addq $16, %r12                      #   %r12 = nodo_tamanho_bloco + 16
        addq %r12, %r13                     #   nodo = nodo_seguinte
        jmp while
    end_while:                              # } while (nodo != TOP_HEAP)
    movq $(STR_ENDL), %rdi
    call printf
    movq $(STR_ENDL), %rdi
    call printf
    popq %rbp                               # restaura %rbp
    ret                                     # restaura %rip
