#ifndef HEAP_MANAGER_H
#define HEAP_MANAGER_H

void iniciaAlocador();

void finalizaAlocador();

void *alocaMem(long int num_bytes);

int liberaMem(void *bloco);

void imprimeMapa();

#endif /* HEAP_MANAGER_H */